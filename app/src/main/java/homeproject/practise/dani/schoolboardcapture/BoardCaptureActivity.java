package homeproject.practise.dani.schoolboardcapture;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import static android.os.Environment.getExternalStorageDirectory;

public class BoardCaptureActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_board_capture);

        try {
            File imageFile = new File(getExternalStorageDirectory().getAbsolutePath()
                    + "/tmp_image.jpg");
            FileInputStream fis = new FileInputStream(imageFile);
            Bitmap img = BitmapFactory.decodeStream(fis);
            ImageView ivBoardCapture = (ImageView) findViewById(R.id.ivCapturedPicture);
            ivBoardCapture.setImageBitmap(img);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }
}
