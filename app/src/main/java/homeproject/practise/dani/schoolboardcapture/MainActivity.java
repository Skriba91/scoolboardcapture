package homeproject.practise.dani.schoolboardcapture;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import static android.os.Environment.getExternalStorageDirectory;

public class MainActivity extends AppCompatActivity {

    private static final String SUBJECT = "SUBJECT: ";
    private static final String SUBJECTNONE = "SUBJECT: NONE";
    private static final int BOARD_CAPTURE = 1;
    private static final int CAMERA_IMAGE_REQUEST = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        String[] proba = {"Egy", "Kettő"};

        Spinner spinner = (Spinner) findViewById(R.id.spinnerCurrentSubject);
        ArrayAdapter<CharSequence> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item);
        adapter.add(proba[0]);
        adapter.add(proba[1]);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        Log.i("SPINNER", spinner.getSelectedItem().toString());
        //TODO Képernyőforgatás után a spinner visszaáll az alappozícióra, de a szöveg marad!

        Button btnCapture = (Button) findViewById(R.id.btnCapture);
        btnCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File imageFile = new File(getExternalStorageDirectory().getAbsolutePath()
                        + "/tmp_image.jpg");
                Uri imageFileUri = Uri.fromFile(imageFile);
                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                takePicture.putExtra(MediaStore.EXTRA_OUTPUT, imageFileUri);
                startActivityForResult(takePicture, CAMERA_IMAGE_REQUEST);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == CAMERA_IMAGE_REQUEST) {
            if(resultCode == RESULT_OK) {
                Intent boardCapture = new Intent(getApplicationContext(),
                        BoardCaptureActivity.class);
                startActivity(boardCapture);
            }
            else {
                Toast.makeText(this, getString(R.string.txtPhotoCancelled),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }



}
